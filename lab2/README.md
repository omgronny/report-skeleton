# Лабораторная работа 2

**Название:** "Разработка драйверов блочных устройств"

**Цель работы:** 

Получить знания и навыки разработки драйверов блочных устройств для операционной системы Linux. 

## Описание функциональности драйвера

Драйвер создаёт виртуальный жесткий диск в оперативной  памяти с размером 50 Мбайт. 
Созданный диск должен быть разбит на один первичный раздел размером 10Мбайт и один  расширенный раздел, содержащий два логических раздела  размером 20Мбайт каждый.

## Инструкция по сборке
**требуемая версия ядра:** 5.15

Выполнить
```bash
make
```

## Инструкция пользователя

для загрузки модуля выпонлить 
```bash
sudo insmod lab2.ko
```

для выгрузки модуля 
```bash
sudo rmmod lab2
```


## Примеры использования
Выполнить команду `lsblk`

Ожидаемый результат
```bash
sr0          11:0    1  1024M  0 rom  
vramdisk    252:0    0    54M  0 disk 
|-vramdisk1 252:1    0    10M  0 part 
|-vramdisk2 252:2    0     1K  0 part 
|-vramdisk5 252:5    0    20M  0 part 
`-vramdisk6 252:6    0    20M  0 part
```


## Тестирование драйвера замером времени копирования

```bash
mkfs.fat 4.2 (2021-01-31)
mkfs.fat 4.2 (2021-01-31)
mkfs.fat 4.2 (2021-01-31)
5+0 records in
5+0 records out
5242880 bytes (5,2 MB, 5,0 MiB) copied, 0,0297006 s, 177 MB/s
5+0 records in
5+0 records out
5242880 bytes (5,2 MB, 5,0 MiB) copied, 0,0289098 s, 181 MB/s
5+0 records in
5+0 records out
5242880 bytes (5,2 MB, 5,0 MiB) copied, 0,0299993 s, 175 MB/s
Copying files within virtual disk
5,00MiB 0:00:00 [ 379MiB/s] [===========================================================================================================================================================>] 100%            
5,00MiB 0:00:00 [ 507MiB/s] [===========================================================================================================================================================>] 100%            
5,00MiB 0:00:00 [ 431MiB/s] [===========================================================================================================================================================>] 100%            
5+0 records in
5+0 records out
5242880 bytes (5,2 MB, 5,0 MiB) copied, 0,029871 s, 176 MB/s
5+0 records in
5+0 records out
5242880 bytes (5,2 MB, 5,0 MiB) copied, 0,0279016 s, 188 MB/s
5+0 records in
5+0 records out
5242880 bytes (5,2 MB, 5,0 MiB) copied, 0,0263265 s, 199 MB/s
Copying files from virtual file to real disk
5,00MiB 0:00:00 [ 474MiB/s] [===========================================================================================================================================================>] 100%            
5,00MiB 0:00:00 [ 784MiB/s] [===========================================================================================================================================================>] 100%            
5,00MiB 0:00:00 [ 468MiB/s] [===========================================================================================================================================================>] 100%
```
