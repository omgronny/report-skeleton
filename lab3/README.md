# Лабораторная работа №3

### Вариант
Перехватываемый трафик: Пакеты протокола ICMP (Internet Control Message Protocol) – только тип 8. Вывести данные. 

Состояние разбора пакетов необходимо выводить в файл в директории `/proc`


## Инструкция по сборке

**Требуемая версия ядра:** 4.15

Загрузка модуля: 

```bash
sudo make load
```

```bash
make watch
```

```bash
[ 1327.955154] Module main loaded
[ 1327.955155] main: create link vni0
[ 1327.955156] main: registered rx handler for enp0s3
[ 1327.983799] IPv6: ADDRCONF(NETDEV_UP): vni0: link is not ready
[ 1327.983835] main: device opened: name=vni0
```

Проверяем созданный сетевой интерфейс 
```bash
ip a
```
```bash
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:58:ee:60 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 84867sec preferred_lft 84867sec
    inet6 fe80::c166:6131:9254:4736/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
3: lab3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN group default qlen 1000
    link/ether 08:00:27:58:ee:60 brd ff:ff:ff:ff:ff:ff
```


```bash
ls /proc | grep lab3
```

## Пример использования

```shell
ping -c 1 -p 506f73746176746520 -s 8 10.0.2.15
ping -c 1 -p 6d6178696d756d -s 7 10.0.2.15
ping -c 1 -p 706f696e747320706c7a -s 10 10.0.2.15
```


```shell
cat /proc/lab3

1. lo: 8 bytes:
Bytes: "Postavte"
Hex: 50 6F 73 74 61 76 74 65
2. lo: 7 bytes:
Bytes: "maximum"
Hex: 6D 61 78 69 6D 75 6D
3. lo: 10 bytes:
Bytes: "points plz"
Hex: 70 6F 69 6E 74 73 20 70 6C 7A
Summary: 25 bytes.
```

#### Смотрим статистику принятых пакетов
```shell
ip -s link show lab3
```
```shell
6: lab3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN mode DEFAULT group default qlen 1000
    link/ether 08:00:27:4f:05:98 brd ff:ff:ff:ff:ff:ff
    RX: bytes  packets  errors  dropped overrun mcast   
    109        3        0       0       0       0       
    TX: bytes  packets  errors  dropped carrier collsns 
    0          0        0       0       0   
```


